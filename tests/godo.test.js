import {expect} from 'vitest'
import { domTest } from './helpers.mjs'
import '../src-js/godo.js'

domTest('content is moved from linked source', ({appendToDom}) => {
  // invalid-tag to check initial content is parsed before being assigned to
  // the input value
  const dom = appendToDom(`
    <div>
      <form>
        <textarea hidden id="marshmallow">
          <invalid-tag>Initial Content</invalid-tag>
        </textarea>
      </form>
      <godo-editor linked-source="marshmallow">
      </godo-editor>
    </div>
  `)
  const godoContent = dom.querySelector('godo-editor .godo--editor')
  expect(godoContent.innerHTML).toBe('<p>Initial Content</p>')
})

domTest('godo content is updated from source on update-event', ({appendToDom}) => {
  // invalid-tag to check initial content is parsed before being assigned to
  // the text area value
  const dom = appendToDom(`
    <div>
      <form>
        <textarea hidden id="marshmallow">
        </textarea>
      </form>
      <godo-editor update-event="update-me" linked-source="marshmallow" >
      </godo-editor>
    </div>
  `)

  const godoContent = dom.querySelector('godo-editor .godo--editor')
  const marshmallow = dom.querySelector('#marshmallow')

  marshmallow.value = '<invalid-tag>Content changed</invalid-tag>'
  marshmallow.dispatchEvent(new Event('update-me'))
  expect(godoContent.innerHTML).toBe('<p>Content changed</p>')
})

domTest('linked source value is updated on form submit', ({appendToDom}) => {
  const dom = appendToDom(`
    <div>
      <form>
        <textarea hidden id="marshmallow"></textarea>
      </form>
      <godo-editor update-event="update-me" linked-source="marshmallow">
      </godo-editor>
    </div>
  `)

  const godo = dom.querySelector('godo-editor')
  const marshmallow = dom.querySelector('#marshmallow')

  godo.view.dispatch(godo.state.tr.insertText('Changed content'))
  expect(marshmallow.value).toBe('')

  marshmallow.form.dispatchEvent(new Event('submit'))
  expect(marshmallow.value).toBe('<p>Changed content</p>')
})

domTest('rows attribute', ({appendToDom}) => {
  const dom = appendToDom(`
    <div>
      <textarea hidden id="marshmallow"></textarea>
      <godo-editor linked-source="marshmallow">
      </godo-editor>
    </div>
  `)

  const godo = dom.querySelector('godo-editor')
  expect(godo.view.dom.style.minHeight).toBe('null')

  godo.setAttribute('rows', '4')
  expect(godo.view.dom.style.minHeight).toBe('calc(1em + 4lh)')
})
