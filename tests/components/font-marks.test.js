import {Node} from 'prosemirror-model'
import { expect, test} from 'vitest'

import {loadSchema} from '../../src-js/utils'
import {parse, serialize, TestState} from '../helpers'

import base from '../../src-js/components/base'
import fontMarks from '../../src-js/components/font-marks'

test.each([
  ['<p><i>9A BRULE TR7S BIEN</i></p>', 'em'],
  ['<p><em>9A BRULE TR7S BIEN</em></p>', 'em'],
  ['<p style="font-style: italic;">9A BRULE TR7S BIEN</p>', 'em'],
  ['<p><strong>9A BRULE TR7S BIEN</strong></p>', 'strong'],
  ['<p><strong>9A BRULE TR7S BIEN</strong></p>', 'strong'],
  ['<p style="font-weight: bold;">9A BRULE TR7S BIEN</p>', 'strong'],
  ['<p style="font-weight: 500;">9A BRULE TR7S BIEN</p>', 'strong'],
  ['<p style="font-weight: bolder;">9A BRULE TR7S BIEN</p>', 'strong'],
  ['<p ><b style="font-weight: normal;">9A BRULE TR7S BIEN</b></p>', undefined],
])('parse dom', (source, mark) => {
  const schema = loadSchema([base(), fontMarks()])
  const node = parse(schema, source)
  expect(node.toJSON()).toEqual({
    type: 'doc',
    content: [{
      type: 'paragraph',
      content: [{
        type: 'text',
        text: '9A BRULE TR7S BIEN',
        marks: mark ? [{ type: mark }] : undefined,
      }],
    }],
  })
})

test.each([
  ['em', '<p><em>9A BRULE TR7S BIEN</em></p>'],
  ['strong', '<p><strong>9A BRULE TR7S BIEN</strong></p>'],
])('serialize to dom', (mark, dom) => {
  const schema = loadSchema([base(), fontMarks()])
  const doc = Node.fromJSON(schema, {
    type: 'doc',
    content: [{
      type: 'paragraph',
      content: [{
        type: 'text',
        text: '9A BRULE TR7S BIEN',
        marks: [{ type: mark }],
      }],
    }],
  })

  expect(serialize(schema, doc)).toBe(dom)
})

test.each([
  ['Mod-i', 'em'],
  ['Mod-I', 'em'],
  ['Mod-b', 'strong'],
  ['Mod-B', 'strong'],
])('keymaps', (key, tag) => {
  const state = new TestState([base(), fontMarks()], '<p>VOTER AVEC UN PAV2</p>')
  state.select('PAV2')
  state.key(key)
  expect(state.html).toBe(`<p>VOTER AVEC UN <${tag}>PAV2</${tag}></p>`)
})

test.each([
  ['toggleEm', 'em'],
  ['toggleStrong', 'strong'],
])('marks menu actions', (menu, tag) => {
  const state = new TestState([base(), fontMarks()], '<p>VOTER AVEC UN PAV2</p>')
  state.select('PAV2')
  state.clickMarksMenu(menu)
  expect(state.html).toBe(`<p>VOTER AVEC UN <${tag}>PAV2</${tag}></p>`)
})
