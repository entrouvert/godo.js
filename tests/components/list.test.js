import {Node} from 'prosemirror-model'
import {test, expect } from 'vitest'

import {loadSchema} from '../../src-js/utils'
import {parse, serialize, TestState} from '../helpers'

import base from '../../src-js/components/base'
import list from '../../src-js/components/list'

test('parse dom', () => {
  const schema = loadSchema([base(), list()])
  const node = parse(schema, `
    <ul>
      <li><p>OK</p></li>
      <li><p>NICKEL</p></li>
    </ul>
  `)

  expect(node.toJSON()).toEqual({
    type: 'doc',
    content: [{
      type: 'bulletList',
      content: [
        {
          type: 'listItem',
          content: [{
            type: 'paragraph',
            content: [{type: 'text', text: 'OK' }],
          }],
        },
        {
          type: 'listItem',
          content: [{
            type: 'paragraph',
            content: [{type: 'text', text: 'NICKEL' }],
          }],
        },
      ],
    }],
  })
})

test('serialize to dom', () => {
  const schema = loadSchema([base(), list()])
  const doc = Node.fromJSON(schema, {
    type: 'doc',
    content: [{
      type: 'bulletList',
      content: [
        {
          type: 'listItem',
          content: [{
            type: 'paragraph',
            content: [{type: 'text', text: 'OK' }],
          }],
        },
        {
          type: 'listItem',
          content: [{
            type: 'paragraph',
            content: [{type: 'text', text: 'NICKEL' }],
          }],
        },
      ],
    }],
  })

  expect(serialize(schema, doc)).toBe(
    '<ul>'
      + '<li><p>OK</p></li>'
      + '<li><p>NICKEL</p></li>'
    + '</ul>',
  )
})

test('lift list item', () => {
  const state = new TestState([base(), list()], `
    <ul>
      <li><p>OK</p></li>
      <li><p>NICKEL</p></li>
    </ul>
  `)
  state.cursorAfter('OK')
  state.key('Mod-[')
  expect(state.html).toBe(
    '<p>OK</p>'
    + '<ul>'
      + '<li><p>NICKEL</p></li>'
   + '</ul>',
  )
})

test('split list item', () => {
  const state = new TestState([base(), list()], `
    <ul>
      <li><p>OK</p></li>
    </ul>
  `)
  state.cursorAfter('OK')
  state.key('Enter')
  state.type('NICKEL')
  expect(state.html).toBe(
    '<ul>'
      + '<li><p>OK</p></li>'
      + '<li><p>NICKEL</p></li>'
   + '</ul>',
  )
})

test('set list menu', () => {
  const state = new TestState([base(), list()], `
    <p>OK</p>
    <p>NICKEL</p>
  `)

  state.select('KNICK')
  state.clickBlocksMenu('setList')
  expect(state.html).toBe(
    '<ul>'
      + '<li><p>OK</p></li>'
      + '<li><p>NICKEL</p></li>'
   + '</ul>',
  )

  state.cursorAfter('NICK')
  state.clickBlocksMenu('setList')
  expect(state.html).toBe(
    '<ul>'
      + '<li><p>OK</p></li>'
   + '</ul>'
   + '<p>NICKEL</p>',
  )
})

