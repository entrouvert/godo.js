import {Node} from 'prosemirror-model'
import { test, expect } from 'vitest'

import {loadSchema} from '../../src-js/utils'
import {parse, serialize, TestState} from '../helpers'

import base from '../../src-js/components/base'

test('parse dom', () => {
  const schema = loadSchema([base()])
  const node = parse(schema, '<p>LES N2ONS JOLIS<br>ET LA MUSIQUE G2NIALE</p>')
  expect(node.toJSON()).toStrictEqual({
    type: 'doc',
    content: [{
      type: 'paragraph',
      content: [
        { type: 'text', text: 'LES N2ONS JOLIS' },
        { type: 'hardBreak' },
        { type: 'text', text: 'ET LA MUSIQUE G2NIALE' },
      ],
    }],
  })
})

test('serialize to dom', () => {
  const schema = loadSchema([base()])
  const doc = Node.fromJSON(schema, {
    type: 'doc',
    content: [{
      type: 'paragraph',
      content: [
        { type: 'text', text: 'LES N2ONS JOLIS' },
        { type: 'hardBreak' },
        { type: 'text', text: 'ET LA MUSIQUE G2NIALE' },
      ],
    }],
  })

  expect(serialize(schema, doc)).toBe('<p>LES N2ONS JOLIS<br>ET LA MUSIQUE G2NIALE</p>')
})

test('undo / redo', () => {
  const state = new TestState([base()], '<p>LA ROUTINE</p>')
  state.type(' C4EST TOUJOURS PAREIL')

  state.key('Mod-z')
  expect(state.html).toBe('<p>LA ROUTINE</p>')

  state.key('Shift-Mod-z')
  expect(state.html).toBe('<p>LA ROUTINE C4EST TOUJOURS PAREIL</p>')

  state.key('Mod-z')
  state.key('Mod-y')
  expect(state.html).toBe('<p>LA ROUTINE C4EST TOUJOURS PAREIL</p>')
})

test.each([
  'Mod-Enter',
  'Shift-Enter',
  'Ctrl-Enter',
])('insert hard break', (brKeyMap) => {
  const state = new TestState([base()], '<p>INVESTIR DANS UN</p>')
  state.key(brKeyMap)
  state.type('CHAR D4ASSAUT')
  expect(state.html).toBe('<p>INVESTIR DANS UN<br>CHAR D4ASSAUT</p>')
})

test.each([
  state => state.key('Shift-Ctrl-0'),
  state => state.clickBlocksMenu('setP'),
])('switch block to paragraph', (action) => {
  const state = new TestState([base(), {
    // create a dummy block type, to test switching to paragraph block
    // as only pargraph is declared in base component nodes.
    nodes: {
      punk: {
        content: 'inline*',
        group: 'block',
        parseDOM: [{tag: 'punk'}],
        toDOM () { return ['punk', 0] },
      },
    },
  }], '<punk>C4EST NOUS</punk>')
  expect(state.html).toBe('<punk>C4EST NOUS</punk>')
  action(state)
  expect(state.html).toBe('<p>C4EST NOUS</p>')
})
