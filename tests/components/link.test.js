import {test, expect, vi} from 'vitest'
import {Node} from 'prosemirror-model'

import {loadSchema} from '../../src-js/utils'
import {parse, serialize, TestState} from '../helpers'

import base from '../../src-js/components/base'

const openDialogMock = vi.fn()
vi.mock('../../src-js/godo-dialog.mjs', () => ({
  TextField: vi.fn(),
  openDialog: openDialogMock,
}))

test('parse dom', async () => {
  const {default: link} = await import('../../src-js/components/link')
  const schema = loadSchema([base(), link()])
  const node = parse(schema, '<p><a href="https://entrouvert.com">CLIQUE</a></p>')
  expect(node.toJSON()).toEqual({
    type: 'doc',
    content: [{
      type: 'paragraph',
      content: [{
        type: 'text',
        text: 'CLIQUE',
        marks: [{
          type: 'link',
          attrs: { href: 'https://entrouvert.com/' },
        }],
      }],
    }],
  })
})

test('serialize to dom', async () => {
  const {default: link} = await import('../../src-js/components/link')
  const schema = loadSchema([base(), link()])
  const doc = Node.fromJSON(schema, {
    type: 'doc',
    content: [{
      type: 'paragraph',
      content: [{
        type: 'text',
        text: 'CLIQUE',
        marks: [{
          type: 'link',
          attrs: { href: 'https://entrouvert.com' },
        }],
      }],
    }],
  })

  expect(serialize(schema, doc)).toBe('<p><a href="https://entrouvert.com">CLIQUE</a></p>')
})

test('marks menu actions', async () => {
  const {default: link} = await import('../../src-js/components/link')
  const state = new TestState([base(), link()], '<p>CLIQUE SAPRISTI</p>')

  function mockValidate (url) {
    openDialogMock.mockImplementation(({callback}) => {
      callback('validate', {href: url})
    })
  }

  function mockRemove () {
    openDialogMock.mockImplementation(({callback}) => {
      callback('remove')
    })
  }

  state.select('CLIQUE')
  mockValidate('https://clique.com')
  state.clickMarksMenu('toggleLink')
  expect(state.html).toBe('<p><a href="https://clique.com">CLIQUE</a> SAPRISTI</p>')

  state.select('CLIQUE')
  mockRemove()
  state.clickMarksMenu('toggleLink')
  expect(state.html).toBe('<p>CLIQUE SAPRISTI</p>')

  state.select('CLIQUE')
  mockValidate('https://clique.com')
  state.clickMarksMenu('toggleLink')
  state.select('SAPRISTI')
  mockValidate('https://clique-encore.com')
  state.clickMarksMenu('toggleLink')
  expect(state.html).toBe(
    '<p><a href="https://clique.com">CLIQUE</a> '
    + '<a href="https://clique-encore.com">SAPRISTI</a></p>',
  )

  state.select('LIQUE SAPRI')
  openDialogMock.mockImplementation(() => fail())
  state.clickMarksMenu('toggleLink')
  expect(state.html).toBe('<p>CLIQUE SAPRISTI</p>')
})

