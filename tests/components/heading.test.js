import {Node} from 'prosemirror-model'
import { expect, test } from 'vitest'

import {loadSchema} from '../../src-js/utils'
import {parse, serialize, TestState, domTest} from '../helpers'

import base from '../../src-js/components/base'
import fontMarks from '../../src-js/components/font-marks'
import heading from '../../src-js/components/heading'
import '../../src-js/godo.js'

function headingComponents () {
  // test we can pick non-consecutive headers levels and that the number
  // of levels is capped to 3
  const options = { headingLevels: '2, 3, 5, 6' }
  return [base, fontMarks, heading].map(c => c(options))
}

domTest('Heading levels option', ({appendToDom}) => {
  const dom = appendToDom(`
    <div>
      <form>
        <textarea hidden id="marshmallow">
          <h1>Initial Content</h1>
        </textarea>
      </form>
      <godo-editor linked-source="marshmallow" heading-levels="4,5,6" >
      </godo-editor>
    </div>
  `)
  const godoHeadingLevels = dom.querySelector('godo-editor').options.headingLevels
  expect(godoHeadingLevels).toBe('4,5,6')
})

test.each([
  ['<h1>LES TESTS UNITAIRES</h1>', undefined, undefined],
  ['<h2>LES TESTS UNITAIRES</h2>', 2, undefined],
  ['<h3>LES TESTS UNITAIRES</h3>', 3, undefined],
  ['<h4>LES TESTS UNITAIRES</h4>', undefined, undefined],
  ['<h5>LES TESTS UNITAIRES</h5>', 5, undefined],
  ['<h6>LES TESTS UNITAIRES</h6>', undefined, undefined],
  ['<h2><em>LES TESTS UNITAIRES<em></h2>', 2, 'em'],
  ['<h2><strong>LES TESTS UNITAIRES<strong></h2>', 2, undefined],
])('parse dom', (dom, level, mark) => {
  const schema = loadSchema(headingComponents())
  const node = parse(schema, dom)
  expect(node.toJSON()).toEqual({
    type: 'doc',
    content: [{
      type: level ? 'heading' : 'paragraph',
      attrs: level ? { level } : undefined,
      content: [{
        type: 'text',
        text: 'LES TESTS UNITAIRES',
        marks: mark ? [{ type: mark }] : undefined,
      }],
    }],
  })
})

test('serialize to dom', () => {
  const schema = loadSchema(headingComponents())
  const doc = Node.fromJSON(schema, {
    type: 'doc',
    content: [{
      type: 'heading',
      attrs: {level: 3},
      content: [{
        type: 'text',
        text: 'C4EST LONG',
      }],
    }],
  })

  expect(serialize(schema, doc)).toBe('<h3>C4EST LONG</h3>')
})

test.each([
  ['Shift-Ctrl-1', 2],
  ['Shift-Ctrl-2', 3],
  ['Shift-Ctrl-3', 5],
])('keymaps', (key, level) => {
  const state = new TestState(headingComponents(), '<p>TR7S</p><p>LONG 0 2CRIRE</p>')
  state.key(key)
  expect(state.html).toBe(`<p>TR7S</p><h${level}>LONG 0 2CRIRE</h${level}>`)
})

test.each([
  ['setHeader2', 2],
  ['setHeader3', 3],
  ['setHeader5', 5],
])('marks menu actions', (menu, level) => {
  const state = new TestState(headingComponents(), '<p>TR7S</p>LONG 0 2CRIRE</p>')
  state.clickBlocksMenu(menu)
  expect(state.html).toBe(`<p>TR7S</p><h${level}>LONG 0 2CRIRE</h${level}>`)
})
