GODO
a ProseMirror editor for Publik
=======

Godo is based on proseMirror
ProseMirror is distributed via npm.
You need nodeJS & npm

First install node modules since this folder

`npm install`

Debian
------

`apt install rollup npm node-rollup-plugin-buble node-rollup-plugin-node-resolve`


NPM scripts 
-----

`npm run build`

Get all JS modules and create a big JS file in dist/js

> npm run watch

automatique build when JS is modified


Use Godo
-----

import in your webpage 

* dist/css/godo.css
* dist/js.godo.min.js

Transform a textarea in godo

  <script type="module">
    import Godo from "./js/godo.js";

    const myGodo = new Godo(textarea, {
      schema: "basic"
    });
  </script>

For more examples, see dist/index.html.

Options
-----

Object passed as 2nd parameter

* `schema`: string / Choose schema that activates the possibilities of the editor.
** `basic`: very simple editing: paragraphe, bold & italic.
** `headers`: basic schema with titles.
* `heading_levels`: array / Specifies which heading levels are supported. 3 levels max. Default `[3, 4]`.


License
-------

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>.
