import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    include: 'tests/**/*.test.js',
    watchExclude: ['**'],
    alias: {
      godo: fileURLToPath(new URL('./src-js', import.meta.url)),
    },
    environment: 'happy-dom',
  },
})

