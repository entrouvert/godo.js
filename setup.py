#! /usr/bin/env python3

import os.path
import shutil
import subprocess

from setuptools import Command, setup, find_namespace_packages
from setuptools.command.build import build as _build
from setuptools.command.sdist import sdist as _sdist
from xstatic.pkg import godo as xs


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result.replace('.dirty', '+dirty')
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class compile_nodejs(Command):
    description = 'compile scss files into css files'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        if os.path.exists('dist/css'):
            subprocess.check_call('npm clean-install', shell=True)
            subprocess.check_call('npm run build', shell=True)
            if os.path.exists('xstatic/pkg/godo/data'):
                shutil.rmtree('xstatic/pkg/godo/data')
            shutil.copytree('dist', 'xstatic/pkg/godo/data/', ignore=shutil.ignore_patterns('*.gz'))


class build(_build):
    sub_commands = [('compile_nodejs', None)] + _build.sub_commands


class sdist(_sdist):
    sub_commands = [('compile_nodejs', None)] + _sdist.sub_commands

    def run(self):
        print("creating VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        _sdist.run(self)
        print("removing VERSION file")
        if os.path.exists('VERSION'):
            os.remove('VERSION')


setup(
    name=xs.PACKAGE_NAME,
    version=get_version(),
    description=xs.DESCRIPTION,
    classifiers=xs.CLASSIFIERS,
    keywords=xs.KEYWORDS,
    maintainer=xs.MAINTAINER,
    maintainer_email=xs.MAINTAINER_EMAIL,
    license=xs.LICENSE,
    url=xs.HOMEPAGE,
    platforms=xs.PLATFORMS,
    packages=find_namespace_packages(include=['xstatic.*', 'xstatic.pkg.*']),
    include_package_data=True,
    package_data={
        'xstatic.pkg.godo': ['data/*'],
    },
    zip_safe=False,
    install_requires=[],  # nothing! :)
    # if you like, you MAY use the 'XStatic' package.
    cmdclass={
        'build': build,
        'sdist': sdist,
        'compile_nodejs': compile_nodejs,
    },
)
