VERSION= `git describe | sed 's/^v//; s/-/./g'`
CHANGELOG_VERSION = 0.1
NAME = godo.js

all:

DIST_FILES = dist/js/godo.js dist/js/godo.min.js dist/css/godo.css dist/js/godo.js.map dist/js/godo.min.js.map

clean:
	rm -rf sdist
	rm -rf xstatic/pkg/godo/data

build:
	npm audit --audit-level=high --omit=dev
	npm clean-install --no-audit
	npm run build

dist: clean build
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

orig: dist-bzip2
	mv sdist/$(NAME)-$(VERSION).tar.bz2 ../$(NAME)_$(CHANGELOG_VERSION).orig.tar.bz2

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
