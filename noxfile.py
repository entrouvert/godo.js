import nox


@nox.session
def tests(session):
    session.install('nodeenv')
    session.run('nodeenv', '--prebuilt', '--python-virtualenv')
    session.run('npm', 'clean-install')
    session.run('npx', 'vitest', '--run')


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
        'dist/js/*.js',
        'dist/js/*.js.map',
        'xstatic/pkg/godo/data/js/*.js',
        'xstatic/pkg/godo/data/js/*.js.map',
        'xstatic/pkg/godo/data/index.html',
        'xstatic/pkg/godo/data/css/godo.css',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
