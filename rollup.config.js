import { nodeResolve } from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'

export default {
  input: './src-js/godo.js',
  output: [
    {
      file: 'dist/js/godo.js',
      format: 'es',
      sourcemap: true,
    },
    {
      file: 'dist/js/godo.min.js',
      format: 'es',
      sourcemap: true,
      plugins: [terser()],
    },
  ],
  plugins: [nodeResolve()],
}
