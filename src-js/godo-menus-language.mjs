// menus language
const dLanguage = document.documentElement.lang.split('-')[0]
const nLanguage = navigator.languages ? navigator.languages[0] : navigator.language || navigator.userLanguage
const language = (dLanguage) ? dLanguage : nLanguage
const godoLanguage = (language === 'fr') ? language : 'en'

const languageContent = {
  'a': {
    en: {
      icon: 'a',
      text: 'link',
      dialog: {
        title: 'Link parameter',
        href: 'Link',
      },
    },
    fr: {
      icon: 'a',
      text: 'lien',
      dialog: {
        title: 'Paramètre du lien',
        href: 'Lien',
      },
    },
  },
  'b': {
    en: {
      icon: 'B',
      text: 'bold',
    },
    fr: {
      icon: 'G',
      text: 'gras',
    },
  },
  'cancel': {
    en: {
      text: 'cancel',
    },
    fr: {
      text: 'annuler',
    },
  },
  'h': {
    en: {
      icon: 'H',
      text: 'Headline',
    },
    fr: {
      icon: 'T',
      text: 'Titre',
    },
  },
  'hh': {
    en: {
      icon: 'h',
      text: 'Subhead',
    },
    fr: {
      icon: 't',
      text: 'Sous-titre',
    },
  },
  'hhh': {
    en: {
      icon: 'hh',
      text: 'Crosshead',
    },
    fr: {
      icon: 'tt',
      text: 'Intertitre',
    },
  },
  'i': {
    en: {
      icon: 'i',
      text: 'italic',
    },
    fr: {
      icon: 'i',
      text: 'italique',
    },
  },
  'edit': {
    en: {
      text: 'edit',
    },
    fr: {
      text: 'éditer',
    },
  },
  'ok': {
    en: {
      text: 'validate',
    },
    fr: {
      text: 'valider',
    },
  },
  'p': {
    en: {
      icon: 'p',
      text: 'paragraph',
    },
    fr: {
      icon: 'p',
      text: 'paragraphe',
    },
  },
  'remove': {
    en: {
      text: 'remove',
    },
    fr: {
      text: 'supprimer',
    },
  },
  'ul': {
    en: {
      icon: '•',
      text: 'bullet list',
    },
    fr: {
      icon: '•',
      text: 'Liste à puce',
    },
  },
}

export default function getLanguage (content) {
  return languageContent[content][godoLanguage]
}
