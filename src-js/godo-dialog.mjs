import dialogPolyfill from 'dialog-polyfill'
import getLanguage from './godo-menus-language.mjs'

const renderEl = () => {
  const tplString = `
    <dialog id="godo-dialog" class="godo-dialog">
      <h2 id="godo-dialog--title" class="godo-dialog--title"></h2>
      <form id="godo-dialog--form" class="godo-dialog--form" method="dialog">
        <div id="godo-dialog--content"></div>
        <menu class="godo-dialog--menu">
          <li class="godo-dialog--menuitem">
            <button id="godo-dialog--valid" class="godo-dialog--valid" value="validate">
              ${getLanguage('ok').text}
            </button>
          </li>
          <li class="godo-dialog--menuitem">
            <button id="godo-dialog--remove" class="godo-dialog--remove" type="button" value="remove">
              ${getLanguage('remove').text}
            </button>
          </li>
          <li class="godo-dialog--menuitem">
            <button id="godo-dialog--cancel" class="godo-dialog--cancel" type="button" value="cancel">
              ${getLanguage('cancel').text}
            </button>
          </li>
        </menu>
      </form>
    </dialog>
  `

  const wrapper = document.createElement('div')
  wrapper.innerHTML = tplString
  return wrapper.firstElementChild
}

const dialog = renderEl()
dialogPolyfill.registerDialog(dialog)

const titleEl = dialog.querySelector('#godo-dialog--title')
const form = dialog.querySelector('#godo-dialog--form')
const cancelBtn = dialog.querySelector('#godo-dialog--cancel')
const removeBtn = dialog.querySelector('#godo-dialog--remove')
const content = dialog.querySelector('#godo-dialog--content')

let fieldsStore
let callbackStore

class TextField {
  constructor (options) {
    const defaultOptions = {
      name: '',
      label: 'label undefined',
      value: '',
      placeholder: '',
      required: false,
    }
    this.options = Object.assign(defaultOptions, options)

    if (!this.options.name) {
      console.error('TextField need a name')
      return
    }

    return this.render()
  }
  render () {
    const o = this.options
    const tplString = `
      <label>
        ${o.label}
        <input type="text"
          name="${o.name}"
          value="${o.value}"
          ${o.required ? 'required' : ''}
          placeholder="${o.placeholder}">
        </input>
      </label>`
    let field = document.createElement('p')
    field.innerHTML = tplString
    return field
  }
}

const openDialog = ({title, fields, callback}) => {
  fieldsStore = fields
  callbackStore = callback
  titleEl.innerText = title
  for (const field in fields) {
    content.appendChild(fields[field])
  }
  dialog.showModal()
}

dialog.addEventListener('keydown', (e) => {
  if (dialog.open && e.key === 'Escape') {
    dialog.returnValue = 'cancel'
  }
})

dialog.addEventListener('close', () => {
  const formdata = new FormData(form)
  let attrData = {}
  for (const field in fieldsStore) {
    attrData[field] = formdata.get(field)
  }
  callbackStore(dialog.returnValue, attrData)

  content.innerHTML = ''
  titleEl.innerText = ''
})

cancelBtn.addEventListener('click', () => {
  dialog.close('cancel')
})

removeBtn.addEventListener('click', () => {
  dialog.close('remove')
})

export {dialog, openDialog, TextField}
