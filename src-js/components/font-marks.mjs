import {toggleMark} from 'prosemirror-commands'
import {icon} from '../utils.mjs'

export default () => ({
  marks: {
    // :: MarkSpec An emphasis mark. Rendered as an `<em>` element.
    // Has parse rules that also match `<i>` and `font-style: italic`.
    em: {
      parseDOM: [
        { tag: 'i' },
        { tag: 'em' },
        { style: 'font-style=italic' },
      ],
      toDOM () { return ['em', 0] },
    },
    // :: MarkSpec A strong mark. Rendered as `<strong>`, parse rules
    // also match `<b>` and `font-weight: bold`.
    strong: {
      parseDOM: [
        { tag: 'strong' },
        // This works around a Google Docs misbehavior where
        // pasted content will be inexplicably wrapped in `<b>`
        // tags with a font-weight normal.
        { tag: 'b', getAttrs: node => node.style.fontWeight !== 'normal' && null },
        { style: 'font-weight', getAttrs: value => /^(bold(er)?|[5-9]\d{2,})$/.test(value) && null },
      ],
      toDOM () { return ['strong', 0] },
    },
  },

  // * **Mod-b** for toggling [strong](#schema-basic.StrongMark)
  // * **Mod-i** for toggling [emphasis](#schema-basic.EmMark)
  // * **Mod-`** for toggling [code font](#schema-basic.CodeMark)
  keymap (schema) {
    const strong = schema.marks.strong
    const em = schema.marks.em

    return {
      'Mod-b': toggleMark(strong),
      'Mod-B': toggleMark(strong),
      'Mod-i': toggleMark(em),
      'Mod-I': toggleMark(em),
    }
  },
  marksMenu (schema) {
    const strong = schema.marks.strong
    const em = schema.marks.em

    return {
      toggleEm: { run: toggleMark(em), dom: icon('i'), type: em },
      toggleStrong: { run: toggleMark(strong), dom: icon('b'), type: strong },
    }
  },
})
