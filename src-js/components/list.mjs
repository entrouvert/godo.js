import { chainCommands } from 'prosemirror-commands'
import {findWrapping, liftTarget, canSplit, ReplaceAroundStep} from 'prosemirror-transform'
import {Slice, Fragment, NodeRange} from 'prosemirror-model'

import {icon} from '../utils.mjs'


export default () => ({
  nodes: {
    // A bullet list node spec, represented in the DOM as `<ul>`.
    bulletList: {
      content: 'listItem+',
      group: 'block',
      parseDOM: [{tag: 'ul'}],
      toDOM () { return ['ul', 0] },
    },
    // A list item (`<li>`) spec.
    listItem: {
      content: 'paragraph+',
      parseDOM: [{tag: 'li'}],
      toDOM () { return ['li', 0] },
      defining: true,
    },
  },

  // * **Enter** to split a non-empty textblock in a list item while at
  //   the same time splitting the list item
  // * **Alt-ArrowUp** to `joinUp`
  // * **Alt-ArrowDown** to `joinDown`
  // * **Mod-BracketLeft** to `lift`
  keymap (schema) {
    const listItem = schema.nodes.listItem
    return {
      'Enter': splitListItem(listItem),
      'Mod-[': liftListItem(listItem),
    }
  },
  blocksMenu (schema) {
    const bulletList = schema.nodes.bulletList
    const listItem = schema.nodes.listItem
    return {
      setList: {
        run: chainCommands(wrapInList(bulletList), liftListItem(listItem)),
        dom: icon('ul'),
        type: bulletList,
        active (state) {
          let {$from, $to} = state.selection
          let range = $from.blockRange($to)
          if (range === undefined) {
            return false
          }
          switch (range.parent.type) {
          case bulletList:
          case listItem:
            return true
          default:
            return false
          }
        },
      },
    }
  },
})

// wrapInList, doWrapInList, splitListItem, liftToOuterList, liftOutOfList, sinkListItem
// adapted from https://github.com/ProseMirror/prosemirror-schema-list/blob/master/src/schema-list.js

// :: (NodeType, ?Object) → (state: EditorState, dispatch: ?(tr: Transaction)) → bool
// Returns a command function that wraps the selection in a list with
// the given type an attributes. If `dispatch` is null, only return a
// value to indicate whether this is possible, but don't actually
// perform the change.
function wrapInList (listType, attrs) {
  return function (state, dispatch) {
    let {$from, $to} = state.selection
    let range = $from.blockRange($to); let doJoin = false; let outerRange = range
    if (!range) return false
    // This is at the top of an existing list item
    if (range.depth >= 2 && $from.node(range.depth - 1).type.compatibleContent(listType)
      && range.startIndex === 0) {
      // Don't do anything if this is the top of the list
      if ($from.index(range.depth - 1) === 0) return false
      let $insert = state.doc.resolve(range.start - 2)
      outerRange = new NodeRange($insert, $insert, range.depth)
      if (range.endIndex < range.parent.childCount) {
        range = new NodeRange($from, state.doc.resolve($to.end(range.depth)), range.depth)
      }
      doJoin = true
    }
    let wrap = findWrapping(outerRange, listType, attrs, range)
    if (!wrap) return false
    if (dispatch) dispatch(doWrapInList(state.tr, range, wrap, doJoin, listType).scrollIntoView())
    return true
  }
}

function doWrapInList (tr, range, wrappers, joinBefore, listType) {
  let content = Fragment.empty
  for (let i = wrappers.length - 1; i >= 0; i--) {
    content = Fragment.from(wrappers[i].type.create(wrappers[i].attrs, content))
  }

  tr.step(new ReplaceAroundStep(range.start - (joinBefore ? 2 : 0), range.end, range.start, range.end,
    new Slice(content, 0, 0), wrappers.length, true))

  let found = 0
  for (let i = 0; i < wrappers.length; i++) if (wrappers[i].type === listType) found = i + 1
  let splitDepth = wrappers.length - found

  let splitPos = range.start + wrappers.length - (joinBefore ? 2 : 0); let parent = range.parent
  for (let i = range.startIndex, e = range.endIndex, first = true; i < e; i++, first = false) {
    if (!first && canSplit(tr.doc, splitPos, splitDepth)) {
      tr.split(splitPos, splitDepth)
      splitPos += 2 * splitDepth
    }
    splitPos += parent.child(i).nodeSize
  }
  return tr
}

// :: (NodeType) → (state: EditorState, dispatch: ?(tr: Transaction)) → bool
// Build a command that splits a non-empty textblock at the top level
// of a list item by also splitting that list item.
function splitListItem (itemType) {
  return function (state, dispatch) {
    let {$from, $to, node} = state.selection
    if ((node && node.isBlock) || $from.depth < 2 || !$from.sameParent($to)) return false
    let grandParent = $from.node(-1)
    if (grandParent.type !== itemType) return false
    if ($from.parent.content.size === 0 && $from.node(-1).childCount === $from.indexAfter(-1)) {
      // In an empty block. If this is a nested list, the wrapping
      // list item should be split. Otherwise, bail out and let next
      // command handle lifting.
      if ($from.depth === 2 || $from.node(-3).type !== itemType
          || $from.index(-2) !== $from.node(-2).childCount - 1) return false
      if (dispatch) {
        let wrap = Fragment.empty
        let depthBefore = $from.index(-1) ? 1 : $from.index(-2) ? 2 : 3
        // Build a fragment containing empty versions of the structure
        // from the outer list item to the parent node of the cursor
        for (let d = $from.depth - depthBefore; d >= $from.depth - 3; d--) {
          wrap = Fragment.from($from.node(d).copy(wrap))
        }
        let depthAfter = $from.indexAfter(-1) < $from.node(-2).childCount ? 1
          : $from.indexAfter(-2) < $from.node(-3).childCount ? 2 : 3
        // Add a second list item with an empty default start node
        wrap = wrap.append(Fragment.from(itemType.createAndFill()))
        let start = $from.before($from.depth - (depthBefore - 1))
        let tr = state.tr.replace(start, $from.after(-depthAfter), new Slice(wrap, 4 - depthBefore, 0))
        let sel = -1
        tr.doc.nodesBetween(start, tr.doc.content.size, (node, pos) => {
          if (sel > -1) return false
          if (node.isTextblock && node.content.size === 0) sel = pos + 1
        })
        if (sel > -1) tr.setSelection(state.selection.constructor.near(tr.doc.resolve(sel)))
        dispatch(tr.scrollIntoView())
      }
      return true
    }
    let nextType = $to.pos === $from.end() ? grandParent.contentMatchAt(0).defaultType : null
    let tr = state.tr.delete($from.pos, $to.pos)
    let types = nextType && [null, {type: nextType}]
    if (!canSplit(tr.doc, $from.pos, 2, types)) return false
    if (dispatch) dispatch(tr.split($from.pos, 2, types).scrollIntoView())
    return true
  }
}

// :: (NodeType) → (state: EditorState, dispatch: ?(tr: Transaction)) → bool
// Create a command to lift the list item around the selection up into
// a wrapping list.
function liftListItem (itemType) {
  return function (state, dispatch) {
    let {$from, $to} = state.selection
    let range = $from.blockRange($to, node => node.childCount && node.firstChild.type === itemType)
    if (!range) return false
    if (!dispatch) return true
    if ($from.node(range.depth - 1).type === itemType) { // Inside a parent list
      return liftToOuterList(state, dispatch, itemType, range)
    } else { // Outer list node
      return liftOutOfList(state, dispatch, range)
    }
  }
}

function liftToOuterList (state, dispatch, itemType, range) {
  let tr = state.tr; let end = range.end; let endOfList = range.$to.end(range.depth)
  if (end < endOfList) {
    // There are siblings after the lifted items, which must become
    // children of the last item
    tr.step(new ReplaceAroundStep(end - 1, endOfList, end, endOfList,
      new Slice(Fragment.from(itemType.create(null, range.parent.copy())), 1, 0), 1, true))
    range = new NodeRange(tr.doc.resolve(range.$from.pos), tr.doc.resolve(endOfList), range.depth)
  }
  dispatch(tr.lift(range, liftTarget(range)).scrollIntoView())
  return true
}

function liftOutOfList (state, dispatch, range) {
  let tr = state.tr; let list = range.parent
  // Merge the list items into a single big item
  for (let pos = range.end, i = range.endIndex - 1, e = range.startIndex; i > e; i--) {
    pos -= list.child(i).nodeSize
    tr.delete(pos - 1, pos + 1)
  }
  let $start = tr.doc.resolve(range.start); let item = $start.nodeAfter
  if (tr.mapping.map(range.end) !== range.start + $start.nodeAfter.nodeSize) return false
  let atStart = range.startIndex === 0; let atEnd = range.endIndex === list.childCount
  let parent = $start.node(-1); let indexBefore = $start.index(-1)
  if (!parent.canReplace(indexBefore + (atStart ? 0 : 1), indexBefore + 1,
    item.content.append(atEnd ? Fragment.empty : Fragment.from(list)))) { return false }
  let start = $start.pos; let end = start + item.nodeSize
  // Strip off the surrounding list. At the sides where we're not at
  // the end of the list, the existing list is closed. At sides where
  // this is the end, it is overwritten to its end.
  tr.step(new ReplaceAroundStep(start - (atStart ? 1 : 0), end + (atEnd ? 1 : 0), start + 1, end - 1,
    new Slice((atStart ? Fragment.empty : Fragment.from(list.copy(Fragment.empty)))
      .append(atEnd ? Fragment.empty : Fragment.from(list.copy(Fragment.empty))),
    atStart ? 0 : 1, atEnd ? 0 : 1), atStart ? 0 : 1))
  dispatch(tr.scrollIntoView())
  return true
}
