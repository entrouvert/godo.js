import {joinUp, joinDown, lift, exitCode, setBlockType, chainCommands} from 'prosemirror-commands'
import {undo, redo} from 'prosemirror-history'
import {isMac, icon} from '../utils.mjs'

export default () => ({
  nodes: {
    // :: NodeSpec The top level document node.
    doc: {
      content: 'block*',
    },
    // :: NodeSpec The text node.
    text: {
      group: 'inline',
    },
    // :: NodeSpec A plain paragraph textblock. Represented in the DOM
    // as a `<p>` element.
    paragraph: {
      content: 'inline*',
      group: 'block',
      parseDOM: [{tag: 'p'}],
      toDOM () { return ['p', 0] },
    },
    // :: NodeSpec A hard line break, represented in the DOM as `<br>`.
    hardBreak: {
      inline: true,
      group: 'inline',
      selectable: false,
      parseDOM: [{tag: 'br'}],
      toDOM () { return ['br'] },
    },
  },

  // * **Ctrl-Shift-0** for making the current textblock a paragraph
  // * **Ctrl-Shift-Backslash** to make the current textblock a code block
  // * **Mod-Enter** to insert a hard break
  keymap (schema) {
    const paragraph = schema.nodes.paragraph
    const hardBreak = schema.nodes.hardBreak

    return {
      'Mod-z': undo,
      'Shift-Mod-z': redo,
      'Mod-y': isMac() ? undefined : redo,
      'Alt-ArrowUp': joinUp,
      'Alt-ArrowDown': joinDown,
      'Mod-BracketLeft': lift,
      'Shift-Ctrl-0': setBlockType(paragraph),
      'Mod-Enter': insertHardBreak(hardBreak),
      'Shift-Enter': insertHardBreak(hardBreak),
      'Ctrl-Enter': isMac() ? undefined : insertHardBreak(hardBreak),
    }
  },
  blocksMenu (schema) {
    const paragraph = schema.nodes.paragraph

    return {
      setP: {
        run: setBlockType(paragraph), dom: icon('p'), type: paragraph,
      },
    }
  },
})

function insertHardBreak (hardBreakNode) {
  return chainCommands(exitCode, (state, dispatch) => {
    dispatch(state.tr.replaceSelectionWith(hardBreakNode.create()).scrollIntoView())
    return true
  })
}
