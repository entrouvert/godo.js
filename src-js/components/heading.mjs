import { setBlockType } from 'prosemirror-commands'
import {icon} from '../utils.mjs'

export default function (options) {
  const levels = (options.headingLevels || '3,4')
    .split(',')
    .map(x => parseInt(x))
    .slice(0, 3)

  return {
    nodes: {
      // :: NodeSpec A heading textblock, with a `level` attribute that
      // should hold the number 1 to 6. Parsed and serialized as `<h1>` to
      // `<h6>` elements.
      heading: {
        attrs: {level: {default: 1}},
        content: 'inline*',
        marks: 'em',
        group: 'block',
        defining: true,
        parseDOM: levels.map(level => ({
          tag: `h${level}`,
          attrs: { level },
        })),
        toDOM (node) { return ['h' + node.attrs.level, 0] },
      },
    },

    // * **Ctrl-Shift-1** to **Ctrl-Shift-Digit6** for making the current
    //   textblock a heading of the corresponding level
    keymap (schema) {
      const heading = schema.nodes.heading
      let keys = {}
      levels.forEach((level, index) => {
        keys[`Shift-Ctrl-${index + 1}`] = setBlockType(heading, {level: level})
      })
      return keys
    },
    blocksMenu ({nodes: {heading}}) {
      let iconId = 'h'
      let menu = {}
      levels.forEach((level) => {
        menu[`setHeader${level}`] = {
          run: setBlockType(heading, {level}),
          dom: icon(iconId),
          type: heading,
          options: {level},
        }
        iconId = iconId + 'h'
      })
      return menu
    },
  }
}

