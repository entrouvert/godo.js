import {toggleMark} from 'prosemirror-commands'
import {icon} from '../utils.mjs'

import { openDialog, TextField } from '../godo-dialog.mjs'
import getLanguage from '../godo-menus-language.mjs'

export default () => ({
  marks: {
    // :: MarkSpec A link mark. `href` attribute.
    // Rendered and parsed as an `<a>` element.
    link: {
      attrs: {href: {}},
      inclusive: false,
      parseDOM: [{
        tag: 'a[href]',
        getAttrs: (node) => ({href: node.href}),
      }],
      toDOM: (node) => ['a', {href: node.attrs.href}, 0],
    },
  },
  marksMenu (schema) {
    const link = schema.marks.link
    return {
      toggleLink: {
        run: editOrToggleLink(link),
        dom: icon('a'),
        type: link,
      },
    }
  },
})


// Edit or Toggle Link. For range only.
// create link or Edit link if link exist in select range
// or remove link if multiple links exists in select range
function editOrToggleLink (linkType) {
  return function (state, dispatch, view) {
    let tr = state.tr
    let $sel = state.selection
    let range = $sel.ranges[0]
    if (!range && $sel.ranges.length > 1) return

    function setLink (linkNodes, attrs) {
      linkNodes.forEach(linkNode => {
        tr.addMark(linkNode.from, linkNode.to, linkType.create(attrs))
      })
      dispatch(tr)
    }

    function unsetLink (linkNodes) {
      linkNodes.forEach(linkNode => {
        tr.removeMark(linkNode.from, linkNode.to, linkType)
      })
      dispatch(tr)
    }

    if (dispatch) {
      const {from, to} = $sel
      let linkNodes = []
      let linkTargets = new Set()
      state.doc.nodesBetween(from, to, (node, pos) => {
        const linkMarks = node.marks.filter(m => linkType === m.type)
        if (linkMarks.length) {
          linkNodes.push({
            node,
            from: pos,
            to: pos + node.nodeSize,
          })
          linkMarks.forEach(m => linkTargets.add(m.attrs.href))
        }
      })

      if (linkTargets.size > 1) {
        unsetLink(linkNodes)
      } else {
        openDialog({
          title: getLanguage('a').dialog.title,
          fields: {
            href: new TextField({
              name: 'href',
              label: getLanguage('a').dialog.href,
              required: true,
              value: linkTargets.values().next().value || '', // get first element of set or empty string
            }),
          },
          callback (dialogValue, attrs) {
            switch (dialogValue) {
            case 'validate':
              if (linkNodes.length) setLink(linkNodes, attrs)
              else toggleMark(linkType, attrs)(state, dispatch)
              break
            case 'remove':
              unsetLink(linkNodes)
              break
            }
            view.focus()
          },
        })
      }
    } else {
      return toggleMark(linkType)(state, null)
    }
    return true
  }
}
