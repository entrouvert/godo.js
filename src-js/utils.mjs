import {Schema} from 'prosemirror-model'
import getLanguage from './godo-menus-language.mjs'

export function loadSchema (components) {
  const schemaSpec = {nodes: {}, marks: {}}
  for (const component of components) {
    Object.assign(schemaSpec.nodes, component.nodes)
    Object.assign(schemaSpec.marks, component.marks)
  }

  return new Schema(schemaSpec)
}

export function isMac () {
  return typeof navigator !== 'undefined' ? /Mac/.test(navigator.platform) : false
}

// Helper function to create menu icons
// id: defined in languageContent
export function icon (id) {
  let menuicon = document.createElement('button')
  menuicon.className = 'menuicon menuicon-' + id
  menuicon.setAttribute('type', 'button')
  menuicon.title = getLanguage(id).text
  menuicon.textContent = getLanguage(id).icon
  return menuicon
}

export function buildKeymap (components, schema) {
  let keys = {}

  for (const component of components) {
    if (component.keymap) {
      Object.assign(keys, component.keymap(schema))
    }
  }

  return keys
}
